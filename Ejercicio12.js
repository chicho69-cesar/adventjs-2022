function selectSleigh(distance, sleighs) {
  let filtered = sleighs.filter(sl => sl.consumption * distance <= 20)
  let result = filtered.at(-1)
  return result ? result.name : null
}

const distance = 30
const sleighs = [
  { name: "Dasher", consumption: 0.3 },
  { name: "Dancer", consumption: 0.5 },
  { name: "Rudolph", consumption: 0.7 },
  { name: "Midu", consumption: 1 }
]

console.log(selectSleigh(distance, sleighs))