function carryGifts(gifts, maxWeight) {
  if (!gifts.every(gift => maxWeight >= gift.length)) {
    return []
  }

  let carried = ['']

  for (let i = 0; i < gifts.length; i++) {
    const prevWeight = carried.at(-1).replace(' ', '').length
    
    if ((prevWeight + gifts[i].length) <= maxWeight) {
      carried[carried.length - 1] += ' ' + gifts[i]
      carried[carried.length - 1] = carried[carried.length - 1].trim()
      continue
    }

    carried.push(gifts[i])
  }

  return carried
}

console.log(carryGifts(['game', 'bike', 'book', 'toy'], 10))
console.log(carryGifts(['game', 'bike', 'book', 'toy'], 7))
// carryGifts(['game', 'bike', 'book', 'toy'], 4)
// carryGifts(['toy', 'gamme', 'toy', 'bike'], 6)