function getOptimalPath(path) {
  const optimalPath = path.reverse().reduce((acc, current) => {
    return current.map((value, index) => {
      return value + Math.min(acc[index], acc[index + 1])
    })
  })

  return optimalPath.pop()
}

let path = [
  [0],
  [7, 4],
  [2, 4, 6]
]

let result = getOptimalPath(path)
console.log(result);

result = getOptimalPath([[0], [2, 3]])
console.log(result);

result = getOptimalPath([[0], [3, 4], [9, 8, 1]])
console.log(result);

result = getOptimalPath([[1], [1, 5], [7, 5, 8], [9, 4, 1, 3]])
console.log(result);