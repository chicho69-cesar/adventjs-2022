function fitsInOneBox(boxes) {
  boxes.sort((a, b) => (a.l + a.w + a.h) - (b.l + b.w + b.h))
  let band = true

  console.log(boxes);

  for (let i = 0; i < boxes.length; i++) {
    if (i + 1 === boxes.length) break
    
    let conditionL = boxes[i].l < boxes[i + 1].l
    let conditionW = boxes[i].w < boxes[i + 1].w
    let conditionH = boxes[i].h < boxes[i + 1].h

    band = conditionL && conditionW && conditionH

    if (!band) return false
  }

  return band
}

let result = fitsInOneBox([
  { l: 1, w: 1, h: 1 },
  { l: 2, w: 2, h: 2 }
])

console.log(result);

let boxes = [
  { l: 1, w: 1, h: 1 },
  { l: 2, w: 2, h: 2 },
  { l: 3, w: 1, h: 3 }
]

result = fitsInOneBox(boxes) 

console.log(result)

boxes = [
  { l: 1, w: 1, h: 10 },
  { l: 3, w: 3, h: 12 },
  { l: 2, w: 2, h: 1 },
]

result = fitsInOneBox(boxes) 

console.log(result)