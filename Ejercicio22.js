function checkStepNumbers(systemNames, stepNumbers) {
  return systemNames.every((e, i) => stepNumbers[i] < stepNumbers[
      i + systemNames.slice(i + 1).indexOf(e) + 1
    ] + !(systemNames.lastIndexOf(e) - i)
  )
}

const systemNames = ["tree_1", "tree_2", "house", "tree_1", "tree_2", "house"]
const stepNumbers = [1, 33, 10, 2, 44, 20]

let result = checkStepNumbers(systemNames, stepNumbers)
console.log(result)

result = checkStepNumbers(["tree_1", "tree_1", "house"], [2, 1, 10])
console.log(result);