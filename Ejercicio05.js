function getMaxGifts(giftsCities, maxGifts, maxCities) {
  let diferentWays = [];
  
  diferentWays.push([], [ giftsCities[0] ]);
  giftsCities.shift()

  giftsCities.forEach(gift => {
    const listOfNewGifts = diferentWays.map(way => {
      let option = [ ...way ]
      if (option.length < maxCities) {
        option.push(gift)
      }

      return option
    })

    diferentWays = diferentWays.concat(listOfNewGifts)
  })

  diferentWays.shift()

  const sumMax = Math.max(
    ...diferentWays.map(way => {
      let sum = way.reduceRight((acc, current) => acc + current)
      return sum > maxGifts ? 0 : sum
    })
  )

  return sumMax
}

console.log(getMaxGifts([12, 3, 11, 5, 7], 20, 3)) // 20
console.log(getMaxGifts([50], 15, 1)) // 0
console.log(getMaxGifts([50], 100, 1)) // 50
console.log(getMaxGifts([50, 70], 100, 1)) // 70
console.log(getMaxGifts([50, 70, 30], 100, 2)) // 100
console.log(getMaxGifts([50, 70, 30], 100, 3)) // 100
console.log(getMaxGifts([50, 70, 30], 100, 4)) // 100