function executeCommands(commands) {
  const normalizeNumber = number => {
    return number > 255
      ? number - 256 : number < 0
      ? number + 256 : number
  }

  const actions = {
    MOV: (a, b, registers) => {
      const position1 = Number(a.slice(1))
      const position2 = Number(b.slice(1))
      registers[position2] = a.includes('V')
        ? normalizeNumber(registers[position1])
        : Number(a)
    },
    ADD: (a, b, registers) => {
      const position1 = Number(a.slice(1))
      const position2 = Number(b.slice(1))
      let number = registers[position1] + registers[position2]
      registers[position1] = normalizeNumber(number)
    },
    DEC: (a, _, registers) => {
      const position1 = Number(a.slice(1))
      let number = registers[position1] - 1
      registers[position1] = normalizeNumber(number)
    },
    INC: (a, _, registers) => {
      const position1 = Number(a.slice(1))
      let number = registers[position1] + 1
      registers[position1] = normalizeNumber(number)
    },
    JMP: (a, _, registers, commands, current) => {
      if (registers[0] === 0) return

      commands.slice(a, current + 1).forEach(command => {
        const [ action, args ] = command.split(' ')
        const [ position1, position2 ] = args.split(',')
        
        actions[action](
          position1,
          position2,
          registers,
          commands,
          commands.indexOf(command)
        )
      })
    }
  }

  let registers = [0, 0, 0, 0, 0, 0, 0, 0]

  commands.forEach(command => {
    const [ action, args ] = command.split(' ')
    const [ position1, position2 ] = args.split(',')
    actions[action](
      position1,
      position2,
      registers,
      commands,
      commands.indexOf(command)
    )
  })

  return registers
}

let result

result = executeCommands([
  'MOV 5,V00',  // V00 es 5
  'MOV 10,V01', // V01 es 10
  'DEC V00',    // V00 ahora es 4
  'ADD V00,V01' // V00 = V00 + V01 = 14
])
console.log(result)
// Output: [14, 10, 0, 0, 0, 0, 0]

result = executeCommands([
  'MOV 255,V00', // V00 es 255
  'INC V00',     // V00 es 256, desborda a 0
  'DEC V01',     // V01 es -1, desborda a 255
  'DEC V01'      // V01 es 254
])
console.log(result)
// Output: [0, 254, 0, 0, 0, 0, 0]

result = executeCommands([
  'MOV 10,V00', // V00 es 10
  'DEC V00',    // decrementa V00 en 1  <---┐
  'INC V01',    // incrementa V01 en 1      |
  'JMP 1',      // bucle hasta que V00 sea 0 ----┘
  'INC V06'     // incrementa V06 en 1
])
console.log(result)
// Output: [ 0, 10, 0, 0, 0, 0, 1, 0 ]