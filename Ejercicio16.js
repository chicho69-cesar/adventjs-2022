function fixLetter(letter) {
  const fixedLetter = letter
    .trim()
    .replace(/\s+/g, ' ')
    .replace(/\?+/g, '?')
    .replace(/,\s{0,1}/g, ', ')
    .replace(/([a-z])$/gi, '$1.')
    .replace(/\s{1}([,.\?\!])/g, '$1')
    .replace(/santa claus/gi, 'Santa Claus')
    .replace(/[\?\!.]+\s[a-z]|^[a-z]/gi, c => c.toUpperCase())

  return fixedLetter
}

let result = fixLetter(` hello,  how are you??     do you know if santa claus exists?  i really hope he does!  bye  `)
console.log(result)

result = fixLetter("  Hi Santa claus. I'm a girl from Barcelona , Spain . please, send me a bike.  Is it possible?")
console.log(result)