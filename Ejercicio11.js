function getCompleted(part, total) {
  const gcd = (a, b) =>  b === 0 ? a : gcd(b, a % b)

  const partInSeconds = part
    .split(':')
    .reduce((acc, current, index) => {
      return acc + current * 60 ** (2 - index)
    }, 0)
    
  const totalInSeconds = total
    .split(':')
    .reduce((acc, current, index) => {
      return acc + current * 60 ** (2 - index)
    }, 0)

  const gcdOfPartAndTotal = gcd(partInSeconds, totalInSeconds)
  const numerator = partInSeconds / gcdOfPartAndTotal
  const denominator = totalInSeconds / gcdOfPartAndTotal

  return `${ numerator }/${ denominator }`
}

console.log(getCompleted('01:00:00', '03:00:00')) // '1/3'
console.log(getCompleted('02:00:00', '04:00:00')) // '1/2'
console.log(getCompleted('01:00:00', '01:00:00')) // '1/1'
console.log(getCompleted('00:10:00', '01:00:00')) // '1/6'
console.log(getCompleted('01:10:10', '03:30:30')) // '1/3'
console.log(getCompleted('03:30:30', '05:50:50')) // '3/5