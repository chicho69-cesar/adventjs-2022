function isPalindrome(str) {
  return str.split('').reverse().join('') === str
}

function checkPart(part) {
  let isPalindromePart = isPalindrome(part)
  if (isPalindromePart) return true

  let band = false

  let arr = part.split('')
  let arr2 = [ ...arr ]

  for (let i = 0; i < part.length; i++) {
    arr2.splice(i, 1)
    band = isPalindrome(arr2.join(''))
    if (band) return true
    arr2 = [ ...arr ]
  }

  return band
}

console.log(checkPart("uwu"))
console.log(checkPart("miidim"))
console.log(checkPart("midu"))